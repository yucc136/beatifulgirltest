package com.yuccs.test;

import android.test.ActivityInstrumentationTestCase2;

import com.actionbarsherlock.R.color;
import com.jayway.android.robotium.solo.Solo;
import com.yucc.R;
import com.yucc.ui.DetailActivity;

public class DetailTest extends ActivityInstrumentationTestCase2<DetailActivity> {
	
	Solo solo;

	public DetailTest(Class<DetailActivity> activityClass) {
		super(activityClass);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
	}
	
	protected void testOptionMenuSelect() {
		// TODO Auto-generated method stub
		solo.clickOnMenuItem(getString(R.string.menu_save_to_sdcard));
	}
	
	
	private String getString(int resId){
		return getActivity().getResources().getString(R.string.menu_save_to_sdcard);
	}
	
	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}

}
