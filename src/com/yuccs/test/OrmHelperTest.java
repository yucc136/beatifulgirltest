package com.yuccs.test;

import java.sql.SQLException;

import android.test.AndroidTestCase;

import com.j256.ormlite.dao.Dao;
import com.yucc.data.db.FolderItem;
import com.yucc.data.db.FolderItemManager;

public class OrmHelperTest extends AndroidTestCase {

	FolderItemManager folderItemManager;

	public OrmHelperTest() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		folderItemManager = new FolderItemManager(getContext());
	}
	
	public void testQueryByExample() throws SQLException {
		OrmliteUtils.addData(folderItemManager);

		assertEquals(
				1,
				folderItemManager.getHelper()
						.getFolderItemDao()
						.queryForFieldValues(
								folderItemManager.getMapForEqualQuery(OrmliteUtils.getRecentlyFolderItem(9)))
						.size());

	}

	public void testQueryForMatching() throws SQLException {
		OrmliteUtils.addData(folderItemManager);

		assertEquals(
				1,
				folderItemManager.getHelper().getFolderItemDao()
						.queryForMatching(OrmliteUtils.getFolderItem(9)).size());
	}

	public void testMarkAsRecentlyAndUnique() throws SQLException {
		OrmliteUtils.addData(folderItemManager);
		assertEquals(
				1,
				folderItemManager.getHelper()
						.getFolderItemDao()
						.queryForFieldValues(
								folderItemManager.getMapForEqualQuery(OrmliteUtils.getRecentlyFolderItem(9)))
						.size());

		// mark recently as recently test
		FolderItem item = folderItemManager
				.getFolderItemIfEquals(OrmliteUtils.getRecentlyFolderItem(9));

		assertNotNull(item);

		assertEquals(2, folderItemManager.getRecentlyItems(1).size());

		folderItemManager.getHelper().getFolderItemDao().createOrUpdate(item);
		assertEquals(2, folderItemManager.getRecentlyItems(1).size());

		// mark recentless as recently test
		item = folderItemManager.getFolderItemIfEquals(OrmliteUtils.getFolderItem(8));

		assertNotNull(item);

		folderItemManager.getHelper().getFolderItemDao().createOrUpdate(item);
		assertEquals(3, folderItemManager.getRecentlyItems(1).size());

		assertEquals(
				1,
				folderItemManager.getHelper()
						.getFolderItemDao()
						.queryForFieldValues(
								folderItemManager.getMapForEqualQuery(OrmliteUtils.getFolderItem(8)))
						.size());
	}
	
	public void testStringId() throws SQLException{
		OrmliteUtils.addData(folderItemManager);
		
		FolderItem item = OrmliteUtils.getFavedFolderItem(7);
		assertEquals("one.id", item.id);
		item = folderItemManager.getHelper().getFolderItemDao().queryForSameId(item);
		assertEquals("another.id", item.id);
		assertEquals(1, folderItemManager.getHelper().getFolderItemDao().queryForAll().size());
	}

	public void testQueryByDirectory() throws SQLException {
		OrmliteUtils.addData(folderItemManager);

		final int expect = 1;

		assertEquals("query by directory failed ", expect, folderItemManager
				.getFolderItemsByDirectory("directory/thumbs9",1).size());
	}

	public void testQueryFav() throws SQLException {
		OrmliteUtils.addFavData(folderItemManager);

		final int favExpect = 1;
		assertEquals("query fav failed ", favExpect, folderItemManager.getFavFolderItems(1)
				.size());
	}
	
	public void testQueryRecently() throws SQLException {
		OrmliteUtils.addData(folderItemManager);

		final int recExpect = 2;
		assertEquals("query recently failed ", recExpect, folderItemManager
				.getRecentlyItems(1).size());
	}

	public void testSaveItem() throws SQLException {
		Dao<FolderItem, String> dao = folderItemManager.getHelper().getFolderItemDao();
		dao.create(OrmliteUtils.getFolderItem(0));
		final int expect = 1;

		assertEquals("didn't save " + expect + " items", expect, folderItemManager
				.getHelper().getFolderItemDao().queryForAll().size());
	}

	public void testQueryAll() throws SQLException {
		OrmliteUtils.addData(folderItemManager);

		final int favExpect = 5;
		assertEquals("query fav failed ", favExpect, folderItemManager.getHelper()
				.getFolderItemDao().queryForAll().size());
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		Dao<FolderItem, String> dao = folderItemManager.getHelper().getFolderItemDao();
		dao.delete(dao.deleteBuilder().prepare());
		folderItemManager.release() ;
	}
}
