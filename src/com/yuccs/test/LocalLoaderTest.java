package com.yuccs.test;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;

import android.annotation.SuppressLint;

import com.j256.ormlite.dao.Dao;
import com.yucc.data.db.FolderItem;
import com.yucc.data.db.FolderItemManager;
import com.yucc.loader.LocalLoader;

@SuppressLint("NewApi")
public class LocalLoaderTest extends LoaderTestCase {

	FolderItemManager folderItemManager;

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		folderItemManager = new FolderItemManager(getContext());
	}

	public LocalLoaderTest() {
		// TODO Auto-generated constructor stub
	}

	public void testLoaderFav() throws SQLException {
		addData();
		LocalLoader loader = new LocalLoader(
				getContext(), LocalLoader.CATEGORY_FAV);
		List<FolderItem> list = getLoaderResultSynchronously(loader);
		assertEquals(2, list.size());
	}
	
	public void testLoaderRecently() throws SQLException {
		addData();
		
		LocalLoader loader = new LocalLoader(
				getContext(), LocalLoader.CATEGORY_RECENTLY);
		List<FolderItem> list = getLoaderResultSynchronously(loader);
		assertEquals(2, list.size());
	}
	
	public void testLoaderDirectory() throws SQLException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		addData();
		
		LocalLoader loader = new LocalLoader(
				getContext(), LocalLoader.CATEGORY_OTHER);
		Field directory = LocalLoader.class.getField("directory");
		directory.setAccessible(true);
		directory.set(loader, "directory/thumbs8");
		List<FolderItem> list = getLoaderResultSynchronously(loader);
		assertEquals(1, list.size());
	}

	private void addData() throws SQLException {
		Dao<FolderItem, String> dao = folderItemManager.getHelper().getFolderItemDao();

		dao.createIfNotExists(getFolderItem(10));
		dao.createIfNotExists(getFolderItem(8));

		dao.createIfNotExists(getFavedFolderItem());
		dao.createIfNotExists(getFavedFolderItem());

		dao.createIfNotExists(getRecentlyFolderItem());
		dao.createIfNotExists(getRecentlyFolderItem());
	}

	private FolderItem getFolderItem(int number) {
		final FolderItem item = new FolderItem();
		item.name = "1.name";
		item.path = "path/thumbs/"+number;
		item.directory = "directory/thumbs" + number;
		return item;
	}

	private FolderItem getFavedFolderItem() {
		final FolderItem item = getFolderItem(9);
		item.isFaved = true;
		return item;
	}

	private FolderItem getRecentlyFolderItem() {
		final FolderItem item = getFolderItem(9);
		item.isRecently = true;
		return item;
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		Dao<FolderItem, String> dao = folderItemManager.getHelper().getFolderItemDao();
		dao.delete(dao.deleteBuilder().prepare());
		folderItemManager.release() ;
	}
	
}
