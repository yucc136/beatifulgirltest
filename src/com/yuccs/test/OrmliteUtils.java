package com.yuccs.test;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.yucc.data.db.FavedFolderItem;
import com.yucc.data.db.FolderItem;
import com.yucc.data.db.FolderItemManager;

public class OrmliteUtils {
	
	public static void addData(FolderItemManager mHelper) throws SQLException {
		Dao<FolderItem, String> dao = mHelper.getHelper().getFolderItemDao();

		dao.createIfNotExists(getFolderItem(10));
		dao.createIfNotExists(getFolderItem(8));

		dao.createIfNotExists(getFavedFolderItem(7));

		dao.createIfNotExists(getRecentlyFolderItem(9));
		dao.createIfNotExists(getRecentlyFolderItem(6));
	}
	public static void addFavData(FolderItemManager mHelper) throws SQLException {
		Dao<FavedFolderItem, String> dao = mHelper.getHelper().getFavedDao();
		
		dao.createIfNotExists(getFavedFolderItem(7));

	}

	public static FolderItem getFolderItem(int number) {
		final FolderItem item = new FolderItem();
		item.name = "1.name";
		item.path = "path/thumbs/" + number;
		item.directory = "directory/thumbs" + number;
		item.id = item.toString();
		return item;
	}

	public static FavedFolderItem getFavedFolderItem(int number) {
		final FavedFolderItem item = new FavedFolderItem(getFolderItem(number)) ;
		return item;
	}

	public static FolderItem getRecentlyFolderItem(int number) {
		final FolderItem item = getFolderItem(number);
		return item;
	}
}
